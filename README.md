# Naismith - Keyboard friendly rich media [Matrix](https://matrix.org/) client

Combine the rich text goodness of Slack or Discord with the keyboard friendliness of irssi or weechat.  
The idea is that you will never have to use the mouse if you don't want to!

For now, see the [Wiki](https://gitlab.com/cpttobi/Naismith/wikis/)